# ! /usr/bin/python
from flask import Flask
import pika
import json
from pymongo import MongoClient


app = Flask(__name__)


def busca_cliente(cpf):
    client = MongoClient('localhost', 27017)
    database = client.local
    cliente_db = database.cliente
    cliente = cliente_db.find_one({"cpf": cpf})
    print(cliente)
    return cliente


def criar_cliente(body):

    cliente = body['cliente']
    cliente_data = busca_cliente(cliente['cpf'])
    if cliente_data is None:
        client = MongoClient('localhost', 27017)
        database = client.local
        cliente_db = database.cliente
        cliente_id = cliente_db.insert_one(cliente).inserted_id
        return cliente_id
    else:
        return cliente_data['_id']


def consome_fila(ch, method, properties, body):
    if 'criarPedido' in method.routing_key:
        criar_cliente(json.loads(body.decode('utf8')))
        ch.basic_ack(delivery_tag=method.delivery_tag)

if __name__ == '__main__':
    credentials = pika.credentials.PlainCredentials('luiz', 'luiz')
    connection = pika.BlockingConnection(
        pika.ConnectionParameters('localhost',
                                  credentials=credentials))
    channel = connection.channel()

    channel.basic_consume(on_message_callback=consome_fila,
                          queue='cliente',
                          auto_ack=False)
    channel.start_consuming()
