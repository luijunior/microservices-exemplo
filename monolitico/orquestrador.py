# ! /usr/bin/python
from flask import request
from flask_restful import Resource
from flask import Flask
from flask_restful import Api
import requests
from flask_cors import CORS


app = Flask(__name__)

api = Api(app)
app.config['PROPAGATE_EXCEPTIONS'] = True
app.config['JSON_AS_ASCII'] = False
CORS(app)


class OrquestratorPedido(Resource):

    url_cliente = 'http://localhost:8085/cliente'
    url_pontos = 'http://localhost:8084/pontos'
    url_pedido = 'http://localhost:8083/pedido'

    def post(self):
        json_data = request.get_json()
        cliente = requests.post(url=OrquestratorPedido.url_cliente,
                                json=json_data['cliente'])
        if cliente.status_code == 204:
            pedido = requests.post(url=OrquestratorPedido.url_pedido,
                                   json=json_data)
            if pedido.status_code == 204:
                pontos = requests.post(url=OrquestratorPedido.url_pontos,
                                       json=json_data)
                if pontos.status_code != 204:
                    return {'msg': 'erro ao criar pedido'}, 500
            else:
                return {'msg': 'erro ao criar pedido'}, 500
        else:
            return {'msg': 'erro ao criar pedido'}, 500
        return None, 200

api.add_resource(OrquestratorPedido, '/')


if __name__ == '__main__':
    app.run('0.0.0.0', 8082)
