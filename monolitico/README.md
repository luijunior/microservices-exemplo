# Dependencias

1. Docker
2. Python3
3. Anaconda

## Startar bancos de dados

### Startar mongoDB

```bash
docker run --name mongo-monolitico -p 27017:27017 -d mongo:3.2.21-jessie
```

## Startar aplicações

```bash
# Carregar o ambiente
conda env create -f environment.yml
source activate microservicos
```

```bash
cd <HOME_APP_DIR>
python app.py
python orquestrador.py
python ./service/cliente.py
python ./service/pedido.py
python ./service/pontos.py
```

### Abrir no browser home de criacao de pedido

http://localhost:8080

### Para verificar criacao dos pedidos

```bash
docker exec -it mongo-monolitico /bin/bash
mongo
use local
# Consultar todos os clientes
db.cliente.find()
# Consultar todos os pedidos
db.pedido.find()
# Remover todos os pedidos
db.pedido.deleteMany({})
# Remover todos os clientes
db.cliente.deleteMany({})
```

### Alguns problemas

1. Compartilhamento de banco
2. Servicos diferentes compartilham regras, desconto consulta pedido e pedido consulta cliente
3. Operacao comprometida no caso de qualquer falha
4. Orquestrador que monta requisicoes
5. Lentidao compromete a operacao
5. Erros comprometem a operacao