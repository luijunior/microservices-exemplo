# ! /usr/bin/python
from flask import request
from flask_restful import Resource
from flask import Flask
from flask_restful import Api
from pymongo import MongoClient

app = Flask(__name__)

api = Api(app)
app.config['PROPAGATE_EXCEPTIONS'] = True
app.config['JSON_AS_ASCII'] = False


class Cliente(Resource):

    def post(self):
        json_data = request.get_json()
        cliente = busca_cliente(json_data['cpf'])
        print(cliente)
        if cliente is None:
            insere_cliente(json_data)
        return None, 204


def busca_cliente(cpf):
    client = MongoClient('localhost', 27017)
    database = client.local
    cliente_db = database.cliente
    cliente = cliente_db.find_one({"cpf": cpf})
    return cliente


def insere_cliente(cliente):
    client = MongoClient('localhost', 27017)
    database = client.local
    cliente_db = database.cliente
    cliente_id = cliente_db.insert_one(cliente).inserted_id
    return cliente_id

api.add_resource(Cliente, '/cliente')

if __name__ == '__main__':
    app.run('0.0.0.0', 8085)
