# ! /usr/bin/python
from flask import request
from flask_restful import Resource
from flask import Flask
from flask_restful import Api
import time
from pymongo import MongoClient


app = Flask(__name__)

api = Api(app)
app.config['PROPAGATE_EXCEPTIONS'] = True
app.config['JSON_AS_ASCII'] = False


class Pontos(Resource):

    def post(self):
        json_data = request.get_json()
        print(json_data)
        cliente = busca_cliente(json_data['cliente']['cpf'])
        if cliente is not None:
            if json_data['pedido']['valor'] > 100:
                # Aplicar pontos
                aplicar_pontos(cliente['_id'],
                               (cliente['pontos'] if 'pontos' in cliente else 0) + 50)
#        time.sleep(10)
        return None, 204


def busca_cliente(cpf):
    client = MongoClient('localhost', 27017)
    database = client.local
    cliente_db = database.cliente
    cliente = cliente_db.find_one({"cpf": cpf})
    return cliente


def aplicar_pontos(id, quantidade_de_pontos):
    client = MongoClient('localhost', 27017)
    database = client.local
    cliente_db = database.cliente
    resultado = cliente_db.update_one(
        {'_id': id},
        {'$set': {'pontos': quantidade_de_pontos}})
    print(resultado)

api.add_resource(Pontos, '/pontos')


if __name__ == '__main__':
    app.run('0.0.0.0', 8084)
